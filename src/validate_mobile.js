const cleanMobilePhone = (mobile_phone) => {
    return mobile_phone.trim().replace('+66', '0').replace(/[^0-9]/g, '')
}

const isOnlyNumber = (mobile_phone) => {
    return /^[0-9]*$/.test(mobile_phone)
}

const isValidMobilePhoneNumber = (mobile_phone) => {
    const clean_phone = cleanMobilePhone(mobile_phone)

    if (!isOnlyNumber(clean_phone)) return false
    if (clean_phone.length !== 10) return false
    if (
        (!clean_phone.startsWith('06')) &&
        (!clean_phone.startsWith('08')) &&
        (!clean_phone.startsWith('09'))
    ) {
        return false
    }

    return clean_phone // return mobile
}

module.exports = {
    isValidMobilePhoneNumber
}
